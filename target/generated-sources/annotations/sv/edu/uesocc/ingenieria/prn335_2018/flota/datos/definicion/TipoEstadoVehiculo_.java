package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.EstadoVehiculo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(TipoEstadoVehiculo.class)
public class TipoEstadoVehiculo_ { 

    public static volatile SingularAttribute<TipoEstadoVehiculo, Integer> idTipoEstadoVehiculo;
    public static volatile SingularAttribute<TipoEstadoVehiculo, String> observaciones;
    public static volatile SingularAttribute<TipoEstadoVehiculo, Boolean> noDisponible;
    public static volatile SingularAttribute<TipoEstadoVehiculo, String> nombre;
    public static volatile ListAttribute<TipoEstadoVehiculo, EstadoVehiculo> estadoVehiculoList;
    public static volatile SingularAttribute<TipoEstadoVehiculo, Boolean> activo;

}