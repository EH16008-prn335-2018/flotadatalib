package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Modelo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(Marca.class)
public class Marca_ { 

    public static volatile SingularAttribute<Marca, String> descripcion;
    public static volatile ListAttribute<Marca, Modelo> modeloList;
    public static volatile SingularAttribute<Marca, Integer> idMarca;
    public static volatile SingularAttribute<Marca, String> nombre;
    public static volatile SingularAttribute<Marca, Boolean> activo;

}