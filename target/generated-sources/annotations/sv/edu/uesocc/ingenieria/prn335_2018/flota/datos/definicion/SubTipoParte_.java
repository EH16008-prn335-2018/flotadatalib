package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Parte;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoParte;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(SubTipoParte.class)
public class SubTipoParte_ { 

    public static volatile SingularAttribute<SubTipoParte, Integer> idSubTipoParte;
    public static volatile SingularAttribute<SubTipoParte, TipoParte> idTipoParte;
    public static volatile SingularAttribute<SubTipoParte, String> observaciones;
    public static volatile SingularAttribute<SubTipoParte, String> nombre;
    public static volatile ListAttribute<SubTipoParte, Parte> parteList;
    public static volatile SingularAttribute<SubTipoParte, Boolean> activo;

}