package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.EstadoReserva;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoUsuario;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Vehiculo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Viaje;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(Reserva.class)
public class Reserva_ { 

    public static volatile SingularAttribute<Reserva, String> cliente;
    public static volatile SingularAttribute<Reserva, String> unidad;
    public static volatile ListAttribute<Reserva, EstadoReserva> estadoReservaList;
    public static volatile SingularAttribute<Reserva, Vehiculo> idVehiculo;
    public static volatile SingularAttribute<Reserva, TipoUsuario> idTipoUsuario;
    public static volatile SingularAttribute<Reserva, Date> fechaSolicitada;
    public static volatile SingularAttribute<Reserva, Viaje> viaje;
    public static volatile SingularAttribute<Reserva, Long> idReserva;
    public static volatile SingularAttribute<Reserva, Date> fechaReserva;

}