package sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoVehiculo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Vehiculo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-11-10T06:59:30")
@StaticMetamodel(EstadoVehiculo.class)
public class EstadoVehiculo_ { 

    public static volatile SingularAttribute<EstadoVehiculo, Date> fecha;
    public static volatile SingularAttribute<EstadoVehiculo, Boolean> estadoActual;
    public static volatile SingularAttribute<EstadoVehiculo, Vehiculo> idVehiculo;
    public static volatile SingularAttribute<EstadoVehiculo, TipoEstadoVehiculo> idTipoEstadoVehiculo;
    public static volatile SingularAttribute<EstadoVehiculo, Long> idEstadoVehiculo;

}